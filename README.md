# Trajectory

## Linear Segments with Parabolic Blends (LSPB)[^1]

```math
q(t) = \begin{cases}
q_i+1/2\ddot{q}_ct^2 & \quad 0 \le t \le t_c \\
q_i+\ddot{q}_ct_c(t-t_c/2) & \quad t_c < t \le t_f - t_c \\
q_f-1/2 \ddot{q}_c(t_f-t)^2 & \quad t_f - t_c < t \le t_f \\
\end{cases}
```

![](./figure/lspb.png)

## Quintic Polynomial Trajectories[^2]

```math
q(t)=a_0+a_1t+a_2t^2+a_3t^3+a_4t^4+a_5t^5
```
![](./figure/quin.png)

## Minimum-jerk trajectory[^3]

Skip

## Simulate

- Python

	`./simulate.py`
- C++
```
mkdir build
cd build
cmake ..
make
./traj.out [CURRENT_POS] [DESIRED_POS]
```





[^1]: Siciliano, Bruno, et al. Robotics: modelling, planning and control. Springer Science & Business Media, 2010.
[^2]: Craig, John J. Introduction to robotics: mechanics and control, 3/E. Pearson Education India, 2009.
[^3]: Flash, Tamar, and Neville Hogan. "The coordination of arm movements: an experimentally confirmed mathematical model." Journal of neuroscience 5.7 (1985): 1688-1703.
[^4]: Shadmehr, Reza, et al. Supplementary documents for "The computational neurobiology of reaching and pointing". MIT press, 2005.
