#include <iostream>
#include <vector>
#include <cmath> // power, sqrt

using namespace std;
int freq = 1000;
double f_time = 5.0;

vector<double> lspb(double cur_pos, double des_pos, double acc_scale)
{
  double rel_pos = des_pos - cur_pos;
  double acc_ = (double)((4.0 * rel_pos / pow(f_time,2.0)) * acc_scale);
  double K = (double)((acc_* pow(f_time,2.0) - 4.0*rel_pos) / acc_);
  double t_b_ = (double)((f_time - sqrt(K))/2);
  int t_b = (int)(t_b_ * freq);
  int len_traj = f_time * freq;
  vector<double> pos;
  pos.reserve(len_traj);
  double pos_;
  for(int t=0; t<len_traj; t++)
  {
    if(t <= t_b)
    {
      pos_ = cur_pos + (double)(acc_ / 2.0) * (double)pow((double)t/freq,2.0);
    }
    else if(t_b < t && t <= (len_traj - t_b))
      pos_ = cur_pos + acc_ * t_b_ * ((double)(t/freq) - (double)t_b_/2);
    else
      pos_ = des_pos - (double)(acc_/2) * pow((f_time - (double)t/freq),2.0);

    pos.push_back(pos_);
  }
  return pos;
}


vector<double> quintic(double cur_pos, double des_pos)
{
  vector<double> a(6);
  double rel_pos = des_pos - cur_pos;
  a = {cur_pos, 0, 0, 
      20.0*rel_pos/(2.0*pow(f_time,3)), 
      -30.0*rel_pos/(2.0*pow(f_time,4)), 
      12.0*rel_pos/(2.0*pow(f_time,5))};

  int len_traj = f_time * freq;
  vector<double> pos;
  pos.reserve(len_traj);
  vector<double> vel(len_traj);
  vector<double> acc(len_traj);
  vector<double> jerk(len_traj);
  double pos_, vel_, acc_, jerk_;

  for(int t=0; t<len_traj; t++)
  {
    pos_ = a[0] + a[1]*((double)t/freq) + a[2]*pow((double)t/freq,2) + a[3]*pow((double)t/freq,3) + a[4]*pow((double)t/freq,4) + a[5]*pow((double)t/freq,5);
    vel_ = a[1] + 2.0*a[2]*((double)t/freq) + 3.0*a[3]*pow((double)t/freq, 2) + 4*a[4]*pow((double)t/freq,3) + 5.0*a[5]*pow((double)t/freq,4);
    acc_ = 2.0*a[2] + 6.0*a[3]*((double)t/freq) + 12.0*a[4]*pow((double)t/freq,2) + 20.0*a[5]*pow((double)t/freq,3);
    jerk_ = 6.0*a[3] + 24.0*a[4]*((double)t/freq) + 60.0*a[5]*pow((double)t/freq,2);
    pos.push_back(pos_);
    vel.push_back(vel_);
    acc.push_back(acc_);
    jerk.push_back(jerk_);
  }
  return pos;
}
  

int main(int argc, char** argv) 
{
    vector<double> pos_quin, pos_lspb;
    cout << argc << endl;
    double cur_pos, des_pos;

    if(argc < 2)
    {
      cur_pos = 0.0;
      des_pos = 10.0;
    }
    else
    {
      cur_pos = atof(argv[1]);
      des_pos = atof(argv[2]);
    }
    pos_quin = quintic(cur_pos, des_pos);
    pos_lspb = lspb(cur_pos, des_pos, 3.0);

    int len = f_time * freq;
    for(int i=0; i<len; i++)
      cout << i+1 << " : " << pos_quin[i] << "[deg]" << endl;
    return 0;
}
