%% Quintic function , Cubic Trajectory

q0=input('Initial Angle = ')
qf=input('Final Angle = ')
tf=input('Final Time = ')

a0=q0;
a3=10*(qf-q0)/tf^3;
a4=15*(q0-qf)/tf^4;
a5=6*(qf-q0)/tf^5;

t=[0:tf/40:tf];
q=a0+a3 * t.^3 + a4 * t.^4 + a5 * t.^5;
dq=3*a3*t.^2+4*a4*t.^3+5*a5*t.^4;
ddq=6*a3*t+12*a4*t.^2+20*a5*t.^3;

subplot(311);
plot(t,q)
title('Cubic trajectory')
ylabel('pose q(t)')
subplot(312)
plot(t,dq)
ylabel('velocity dq(t)')
subplot(313)
plot(t,ddq)
ylabel('acceleration ddq(t)')
xlabel('time(s)')