#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import math


def lspb(current, setpoint, freq, time, acc_scale):
    pos = []
    vel = []
    acc = []
    jerk = []
    timefreq = int(time * freq)

    acc_ = (4 * (setpoint - current) / time ** 2.0) * acc_scale
    K = (acc_ * time ** 2.0 - 4 * (setpoint - current)) / acc_
    t_b_ = (time - math.sqrt(K)) / 2
    t_b = int(t_b_ * freq)

    for t in range(timefreq):
        if t <= t_b:
            pos.append(current + (acc_ / 2) * (t / freq) ** 2.0)
            vel.append(acc_ * (t / freq))
            acc.append(acc_)
            if (t == t_b):
                jerk.append(peak_jerk)
            else:
                jerk.append(0)


        elif (t_b < t) and t <= (timefreq - t_b):
            pos.append(current + acc_ * t_b_ * ((t / freq) - t_b_ / 2))
            vel.append(acc_ * t_b_)
            acc.append(0)
            if t == (timefreq - t_b):
                jerk.append(peak_jerk)
            else:
                jerk.append(0)

        else:
            pos.append(setpoint - acc_ / 2 * (time - t / freq) ** 2.0)
            vel.append(-acc_ * (t / freq - time))
            acc.append(-acc_)
            jerk.append(0)


    return pos, vel, acc, jerk


def quintic(current, setpoint, freq, time):
    a = [current, 0, 0,
         (20.0 * (setpoint - current)) / (2.0 * time ** 3.0),
         (30.0 * (current - setpoint)) / (2.0 * time ** 4.0),
         (12.0 * (setpoint - current)) / (2.0 * time ** 5.0)]
    pos = []
    vel = []
    acc = []
    jerk = []
    timefreq = int(time * freq)

    for t in range(timefreq):
        pos.append(
            a[0] + a[1] * (t / freq) + a[2] * (t / freq) ** 2.0 + a[3] * (t / freq) ** 3.0 + a[4] * (t / freq) ** 4.0 +
            a[5] * (t / freq) ** 5.0)
        vel.append(
            a[1] + 2.0 * a[2] * (t / freq) + 3.0 * a[3] * (t / freq) ** 2.0 + 4 * a[4] * (t / freq) ** 3.0 + 5.0 * a[
                5] * (t / freq) ** 4.0)
        acc.append(
            2.0 * a[2] + 6.0 * a[3] * (t / freq) + 12.0 * a[4] * (t / freq) ** 2.0 + 20.0 * a[5] * (t / freq) ** 3.0)
        jerk.append(6.0 * a[3] + 24.0 * a[4] * (t / freq) + 60.0 * a[5] * (t / freq) ** 2.0)

    return pos, vel, acc, jerk


def mjt(current, setpoint, freq, time):
    pos = []
    vel = []
    acc = []
    jerk = []
    timefreq = int(time * freq)

    for t in range(timefreq):
        pos.append(current + (setpoint - current) * (
                    10.0 * (t / timefreq) ** 3 - 15.0 * (t / timefreq) ** 4 + 6.0 * (t / timefreq) ** 5))
        vel.append((1.0 / time) * (setpoint - current) * (
                    30.0 * (t / timefreq) ** 2.0 - 60.0 * (t / timefreq) ** 3.0 + 30.0 * (t / timefreq) ** 4.0))
        acc.append(((1.0 / time) ** 2.0) * (setpoint - current) * (
                    60.0 * (t / timefreq) - 180.0 * (t / timefreq) ** 2.0 + 120.0 * (t / timefreq) ** 3.0))
        jerk.append(((1.0 / time) ** 3.0) * (setpoint - current) * (
                    60.0 - 360.0 * (t / timefreq) + 360.0 * (t / timefreq) ** 2.0))

    return pos, vel, acc, jerk


freq = 1000
average_velocity = 10
current = 0
setpoint = 10
peak_jerk = 5000
time = (setpoint - current) / average_velocity
print(time)
acc_scale = 3  # Only, LSPB (minimum value: 1)

pos_mjt, vel_mjt, acc_mjt, jerk_mjt = mjt(current, setpoint, freq, time)
pos_quin, vel_quin, acc_quin, jerk_quin = quintic(current, setpoint, freq, time)
pos_lspb, vel_lspb, acc_lspb, jerk_lspb = lspb(current, setpoint, freq, time, acc_scale)

# --- Graph --- #

xaxis = [i / freq for i in range(int(time * freq))]
fig, axes = plt.subplots(2, 2)
axes[0, 0].plot(xaxis, pos_mjt, color='k', label='MJT')
# axes[0, 0].plot(xaxis, pos_quin, color='k', label='Quintic')
axes[0, 0].plot(xaxis, pos_lspb, color='k', linestyle=':', label='LSPB')
axes[0, 0].set_title('Pos')
axes[0, 0].set_ylabel('[mm]')
axes[0, 0].set_xlabel('Time[s]')
axes[0, 0].grid(True)

axes[0, 1].plot(xaxis, vel_mjt, color='k')
# axes[0, 1].plot(xaxis, vel_quin, color='k')
axes[0, 1].plot(xaxis, vel_lspb, color='k', linestyle=':')
axes[0, 1].set_title('Vel')
axes[0, 1].set_ylabel('[mm/s]')
axes[0, 1].set_xlabel('Time[s]')
axes[0, 1].grid(True)

axes[1, 0].plot(xaxis, acc_mjt, color='k')
# axes[1, 0].plot(xaxis, acc_quin, color='k')
axes[1, 0].plot(xaxis, acc_lspb, color='k', linestyle=':')
axes[1, 0].set_title('Acc')
axes[1, 0].set_ylabel('[mm/s^2]')
axes[1, 0].set_xlabel('Time[s]')
axes[1, 0].grid(True)

axes[1, 1].plot(xaxis, jerk_mjt, color='k')
# axes[1, 1].plot(xaxis, jerk_quin, color='k')
axes[1, 1].plot(xaxis, jerk_lspb, color='k', linestyle=':')
axes[1, 1].set_title('Jerk')
axes[1, 1].set_ylabel('[mm/s^3]')
axes[1, 1].set_xlabel('Time[s]')
axes[1, 1].grid(True)

# for ax in axes.flat:
#     if ax.colNum == 0 and ax.rowNum == 0:
#         ax.set(xlabel='Time[s]', ylabel='$deg$')
#     elif ax.colNum == 1 and ax.rowNum == 0:
#         ax.set(xlabel='Time[s]', ylabel='$deg/s$')
#     elif ax.colNum == 0 and ax.rowNum == 1:
#         ax.set(xlabel='Time[s]', ylabel='$deg/s^2$')
#     elif ax.colNum == 1 and ax.rowNum == 1:
#         ax.set(xlabel='Time[s]', ylabel='$deg/s^3$')

lines = []
labels = []

for ax in fig.axes:
    axLine, axLabel = ax.get_legend_handles_labels()
    lines.extend(axLine)
    labels.extend(axLabel)


fig.legend(lines, labels, loc='center')
plt.tight_layout()
plt.savefig('traj.png', dpi=300)
plt.show()